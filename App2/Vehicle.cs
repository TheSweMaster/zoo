﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    public class Vehicle
    {
        private string licenceNumber;
        private string color;
        private string brand;
        private int yearOfProduction;

        public int numberOfDoors { get; set; }

        public Vehicle(string licenceNumber, string color, string brand, int yearOfProduction)
        {
            this.licenceNumber = licenceNumber;
            this.color = color;
            this.brand = brand;
            this.yearOfProduction = yearOfProduction;
        }

        public string getLicenceNumber()
        {
            return this.licenceNumber;
        }

        public void SetLicenceNumber(string licenceNumber)
        {
            this.licenceNumber = licenceNumber;
        }

        public string GetColor()
        {
            return this.color;
        }

        public void SetColor(string color)
        {
            this.color = color;
        }

        public string GetBrand()
        {
            return this.brand;
        }

        public void SetBrand(string brand)
        {
            this.brand = brand;
        }

        public int GetYearOfProduction()
        {
            return this.yearOfProduction;
        }

        public void SetYearOfProduction(int yearOfProduction)
        {
            this.yearOfProduction = yearOfProduction;
        }


    }
}
