﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    public class Motorcycle : Vehicle
    {
        private int cc;

        public Motorcycle(string licenceNumber, string color, string brand, int yearOfProduction, int cc) : base(licenceNumber, color, brand, yearOfProduction)
        {
            this.cc = cc;
        }

        public int GetCc()
        {
            return this.cc;
        }

        public void SetCc(int cc)
        {
            this.cc = cc;
        }
    }
}
