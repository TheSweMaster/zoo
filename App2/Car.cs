﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    public class Car : Vehicle
    {
        private int numberOfDoors;

        public Car(string licenceNumber, string color, string brand, int yearOfProduction, int numberOfDoors) : base(licenceNumber, color, brand, yearOfProduction)
        {
            this.numberOfDoors = numberOfDoors;
            //"Base" Anropar konstruktorn i den andra klassen  och gör då Setters och getters automatiskt.

        }

        public int GetNumberOfDoors()
        {
            return this.numberOfDoors;
        }

        public void SetNumberOfDoors(int numberOfDoors)
        {
            this.numberOfDoors = numberOfDoors;
        }

        public override string ToString()
        {
            return base.ToString();
            {

            }
        }

        /*public Car(string licenceNumber, string color, string brand, int yearOfProduction)
        {
            this.licenceNumber = licenceNumber;
            this.color = color;
            this.brand = brand;
            this.yearOfProduction = yearOfProduction;
        } */


    }
}