﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    class Program
    {
        static void Main(string[] args)
        {

            Car car = new Car("ABC 123", "Red", "Volvo", 2016, 4);
            Motorcycle m1 = new Motorcycle("ASD 543", "Black", "Vespa", 2007, 120);
            List<Vehicle> listOfVehicles = new List<Vehicle>();
            listOfVehicles.Add(car);
            listOfVehicles.Add(m1);

            foreach (var vehicle in listOfVehicles)
            {
                Console.WriteLine(vehicle.getLicenceNumber());
                Console.WriteLine(vehicle.GetColor());
                Console.WriteLine(vehicle.GetBrand());
                Console.WriteLine(vehicle.GetYearOfProduction());

                //if() Anna annan typ av lösning :)
                //Console.WriteLine(vehicle.GetNumberOfDoors());

            }

            Console.WriteLine(car.getLicenceNumber());
            Console.WriteLine(car.GetColor());
            Console.WriteLine(car.GetBrand());
            Console.WriteLine(car.GetYearOfProduction());
            Console.WriteLine(car.GetNumberOfDoors());

            Console.WriteLine(m1.getLicenceNumber());
            Console.WriteLine(m1.GetColor());
            Console.WriteLine(m1.GetBrand());
            Console.WriteLine(m1.GetYearOfProduction());
            Console.WriteLine(m1.GetCc());

        }
    }
}
